#!/usr/bin/python

import matplotlib.pyplot as plt
from prep_terrain_data import makeTerrainData
from class_vis import prettyPicture
import sys
sys.path.append("../tools/")
from my_tools import time_run # my tool to measure time

features_train, labels_train, features_test, labels_test = makeTerrainData()


### the training data (features_train, labels_train) have both "fast" and "slow"
### points mixed together--separate them so we can give them different colors
### in the scatterplot and identify them visually
grade_fast = [features_train[ii][0] for ii in range(0, len(features_train)) if labels_train[ii]==0]
bumpy_fast = [features_train[ii][1] for ii in range(0, len(features_train)) if labels_train[ii]==0]
grade_slow = [features_train[ii][0] for ii in range(0, len(features_train)) if labels_train[ii]==1]
bumpy_slow = [features_train[ii][1] for ii in range(0, len(features_train)) if labels_train[ii]==1]


#### initial visualization
plt.xlim(0.0, 1.0)
plt.ylim(0.0, 1.0)
plt.scatter(bumpy_fast, grade_fast, color = "b", label="fast")
plt.scatter(grade_slow, bumpy_slow, color = "r", label="slow")
plt.legend()
plt.xlabel("bumpiness")
plt.ylabel("grade")
plt.show()
################################################################################


### your code here!  name your classifier object clf if you want the 
### visualization code (prettyPicture) to show you the decision boundary
from sklearn.metrics import accuracy_score
from sys import stdout

def FindAccuracy(clf, features_train, labels_train, features_test, labels_test):
	clf.fit(features_train, labels_train)
	return accuracy_score(labels_test, clf.predict(features_test))

def BruteForceBestK(features_train, labels_train, features_test, labels_test):
	best_k = 3
	best_a = 0.0
	for k in range(5,len(features_train)/2,2):
		stdout.write("Testing k={}\r".format(k))
		stdout.flush()
		clf = KNeighborsClassifier(n_neighbors=k)
		accuracy = FindAccuracy(clf, features_train, labels_train, features_test, labels_test)
		if accuracy > best_a:
			best_a = accuracy
			best_k = k
	print('Found {} to be the best k.'.format(best_k))
	return best_k

def BruteForceBestRF(features_train, labels_train, features_test, labels_test):
	best_e = 10
	best_c = 'gini'
	best_a = 0.0
	for c in 'gini', 'entropy':
		for e in range(10, len(features_train)/2,2):
			stdout.write("Trying {} with {:3} estimators. \r".format(c,e))
			stdout.flush()
			clf = RandomForestClassifier(n_estimators=best_e, criterion=best_c)
			accuracy = FindAccuracy(clf, features_train, labels_train, features_test, labels_test)
			if accuracy > best_a:
				best_e = e
				best_c = c
				best_a = accuracy
	print("Found {} with {} estimators to be the best.".format(best_c, best_e))
	return best_e, best_c

"""Algos to try:
A. k nearest neighbors
B. random forest
C. adaboost
"""
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
classifiers = KNeighborsClassifier, RandomForestClassifier, AdaBoostClassifier
ppbool = False # boolean if prettypicture finished

#~ print("The best k was found to be {}.".format(BruteForceBestK(features_train, labels_train, features_test, labels_test)))

for classifier in classifiers:
	print("Using {}.".format(classifier.__module__))
	clf = classifier()
	if classifier == KNeighborsClassifier:
		clf=classifier(n_neighbors=BruteForceBestK(features_train, labels_train, features_test, labels_test))
	if classifier == RandomForestClassifier:
		n_e, crit = BruteForceBestRF(features_train, labels_train, features_test, labels_test)
		clf = classifier(n_estimators=n_e, criterion=crit)
	
	print("Training time is {:.2f}s.".format(time_run(clf.fit, features_train, labels_train)[0]))
	
	rt, pred = time_run(clf.predict,features_test)
	print("Predict time is {:.2f}s.".format(rt))

	accuracy = accuracy_score(pred, labels_test)
		
	print("The accuracy is {:.1f}%!".format(accuracy*100))
	print("-"*20)

	try:
		prettyPicture(clf, features_test, labels_test, fname="{}.png".format(classifier.__module__))
		ppbool = True
	except NameError:
		pass

import os
if ppbool and os.name == 'posix':
	os.system('xdg-open {}.png'.format(classifier.__module__))
