#!/usr/bin/python

""" 
    Starter code for exploring the Enron dataset (emails + finances);
    loads up the dataset (pickled dict of dicts).

    The dataset has the form:
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }

    {features_dict} is a dictionary of features associated with that person.
    You should explore features_dict as part of the mini-project,
    but here's an example to get you started:

    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000
    
"""

import pickle
from pprint import pprint
import sys

enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))
pprint(enron_data)

total = 0
total_poi = 0
total_sal=0
total_email=0
no_tp = 0
no_tpoi=0

with open('enron_data.txt','w') as of:
	for name in enron_data:
		if 'LAY' in name or 'Skilling'.upper() in name or 'Fastow'.upper() in name:
			print("{}:{}".format(name,enron_data[name]['total_payments']))
		total += 1
		if enron_data[name]['poi'] == 1:
			total_poi += 1
		if enron_data[name]['email_address'] != 'NaN':
			total_email += 1
		if enron_data[name]['salary'] != 'NaN':
			total_sal += 1
		if enron_data[name]['total_payments'] == 'NaN':
			no_tp += 1
			if enron_data[name]['poi'] == "True":
				no_tpoi += 1
		features = 0
		of.write(name)
		of.write('\n')
		of.write('-' * len(name))
		of.write('\n')
		for k,v in enron_data[name].items():
			of.write("{}:{}\n".format(k,v))
			features += 1
		of.write('\\/'*20)
		of.write('\n')
		of.write('\n')
	
print("Total = {} - 1 = {}".format(total, total-1))
print("Features = {}".format(features))
print("Total POI = {}".format(total_poi))
print("Total e-mail = {}".format(total_email))
print("Total salary = {}".format(total_sal))
no_tp += 10
print(str(no_tp))
print("Percent with no total-payments = {:.2f}".format((float(no_tp)/total)*100))
print("POI with no total-payments = {:.2f}".format((float(no_tpoi)/total)*100))
