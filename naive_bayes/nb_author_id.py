#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 1 (Naive Bayes) mini-project. 

    Use a Naive Bayes Classifier to identify emails by their authors
    
    authors and labels:
    Sara has label 0
    Chris has label 1
"""
    

import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess
from my_tools import time_run

### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()

from sklearn.naive_bayes import GaussianNB
clf = GaussianNB()
print("Training time is {:.2}s.".format(time_run(clf.fit, features_train, labels_train)[0]))

rt, pred = time_run(clf.predict,features_test)
print("Predict time is {:.2}s.".format(rt))

from sklearn.metrics import accuracy_score

accuracy = accuracy_score(pred, labels_test)
    
print("The accuracy is {}!".format(accuracy))
