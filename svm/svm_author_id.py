#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess
from my_tools import time_run # my tool to measure time
from numpy import count_nonzero

### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()

#"""Comment this line to use only 10% of the training set
features_train = features_train[:len(features_train)/100] 
labels_train = labels_train[:len(labels_train)/100] 
#"""

#########################################################
### your code goes here ###

#########################################################


from sklearn import svm
for _C in 10000.,:		# used these for the optimization testing of C: 10., 100., 1000., 10000.:
	print("Using C value {}!".format(_C))
	clf = svm.SVC(kernel="rbf", C=_C)
	print("Training time is {:.2f}s.".format(time_run(clf.fit, features_train, labels_train)[0]))
	
	print("Predicting {} emails.".format(len(features_test)))
	rt, pred = time_run(clf.predict,features_test)
	print("Predict time is {:.2f}s.".format(rt))

	from sklearn.metrics import accuracy_score

	accuracy = accuracy_score(pred, labels_test)
		
	print("The accuracy is {:.3f}!".format(accuracy))
	print("-"*20)
	
for result in 10, 26, 50:
	print("Message {} was found to be {}.".format(result, "Sara" if pred[result] == 0 else "Chris"))

print("There were {} emails from Chris and {} from Sara.".format(count_nonzero(pred),pred.size-count_nonzero(pred)))
