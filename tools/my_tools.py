from time import time

def time_run(func, *args):
	t0=time()
	ret = func(*args)
	return time() - t0, ret
